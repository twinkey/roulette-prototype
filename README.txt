# Roulette game prototype

The application is designed to simulate basic game functionalities such as: dragging chips, creating bets , viewing 
winning numer and prize.

I started the work by analyzing the mechanics of the game. First, I programmed the board behavior (highlighting boxes, adding chips and taking bets positions), and later I added a reward calculation system.

If I had more time, I would add:

1.  Mechanics of the roulette wheel
2.  Undo player moves
3.  Pooling chips system
4.  Sound effects
5.  Animations