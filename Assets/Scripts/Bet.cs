﻿using System;
using UnityEngine;

[Serializable]
public class Bet
{
	public Enums.BetType m_betType;
	public int m_number_1, m_number_2, m_number_3, m_number_4, m_number_5, m_number_6;
	public int m_betValue;

	public Bet(int betValue, Enums.BetType betType)
	{
		m_betValue = betValue;
		m_betType = betType;
	}

	public Bet(int betValue, int number_1 = -1, int number_2 = -1, int number_3 = -1, int number_4 = -1, int number_5 = -1, int number_6 = -1, Enums.BetType betType = Enums.BetType.SixLine)
	{
		m_betValue = betValue;
		m_betType = betType;
		m_number_1 = number_1;
		m_number_2 = number_2;
		m_number_3 = number_3;
		m_number_4 = number_4;
		m_number_5 = number_5;
		m_number_6 = number_5;
	}

	public Result CheckBet(int winningNumber)
	{
		switch (m_betType)
		{
			case Enums.BetType.Dozen1:
				return FindNumberInDozen(winningNumber, 1);
			case Enums.BetType.Dozen2:
				return FindNumberInDozen(winningNumber, 2);
			case Enums.BetType.Dozen3:
				return FindNumberInDozen(winningNumber, 3);
			case Enums.BetType.Line1:
				return FindNumberInLine(winningNumber, 1);
			case Enums.BetType.Line2:
				return FindNumberInLine(winningNumber, 2);
			case Enums.BetType.Line3:
				return FindNumberInLine(winningNumber, 3);
			case Enums.BetType.Even:
				return FindNumberInEven(winningNumber);
			case Enums.BetType.Odd:
				return FindNumberInOdd(winningNumber);
			case Enums.BetType.High:
				return FindNumberInHigh(winningNumber);
			case Enums.BetType.Low:
				return FindNumberInLow(winningNumber);
			case Enums.BetType.Red:
				return FindNumberInRed(winningNumber);
			case Enums.BetType.Black:
				return FindNumberInBlack(winningNumber);
			case Enums.BetType.Straight:
				return FindNumberInStraight(winningNumber);
			case Enums.BetType.Split:
				return FindNumberInSplit(winningNumber);
			case Enums.BetType.Street:
				return FindNumberInStreet(winningNumber);
			case Enums.BetType.Corner:
				return FindNumberInCorner(winningNumber);
			case Enums.BetType.SixLine:
				return FindNumberInSixLine(winningNumber);

		}

		return new Result(false, 0);
	}

	int CalculatePrize()
	{
		GameStats gameStats = GameManager.Instance.GameStats;

		if (m_betType == Enums.BetType.Dozen1
			|| m_betType == Enums.BetType.Dozen2
			|| m_betType == Enums.BetType.Dozen3)
		{
			return m_betValue + m_betValue * gameStats.DozenPayout;
		}


		if (m_betType == Enums.BetType.Line1
			|| m_betType == Enums.BetType.Line2
			|| m_betType == Enums.BetType.Line3)
		{
			return m_betValue + m_betValue * gameStats.LinePayout;
		}

		if (m_betType == Enums.BetType.Even)
			return m_betValue + m_betValue * gameStats.EvenPayout;

		if (m_betType == Enums.BetType.Odd)
			return m_betValue + m_betValue * gameStats.OddPayout;

		if (m_betType == Enums.BetType.High)
			return m_betValue + m_betValue * gameStats.HighPayout;

		if (m_betType == Enums.BetType.Low)
			return m_betValue + m_betValue * gameStats.LowPayout;

		if (m_betType == Enums.BetType.Red)
			return m_betValue + m_betValue * gameStats.RedPayout;

		if (m_betType == Enums.BetType.Black)
			return m_betValue + m_betValue * gameStats.BlackPayout;

		if (m_betType == Enums.BetType.Straight)
			return m_betValue + m_betValue * gameStats.StraightPayout;

		if (m_betType == Enums.BetType.Split)
			return m_betValue + m_betValue * gameStats.SplitPayout;

		if (m_betType == Enums.BetType.Corner)
			return m_betValue + m_betValue * gameStats.CornerPayout;

		if (m_betType == Enums.BetType.Street)
			return m_betValue + m_betValue * gameStats.StreetPayout;

		if (m_betType == Enums.BetType.SixLine)
			return m_betValue + m_betValue * gameStats.StreetPayout;

		return -1;
	}

	Result FindNumberInSixLine(int winningNumber)
	{
		Result result = new Result(false, 0);

		if (winningNumber == m_number_1 || winningNumber == m_number_2
		|| winningNumber == m_number_3 || winningNumber == m_number_4
		|| winningNumber == m_number_5 || winningNumber == m_number_6)
		{
			result.win = true;
			result.amount = CalculatePrize();
			return result;
		}
		else
			return result;
	}

	Result FindNumberInCorner(int winningNumber)
	{
		Result result = new Result(false, 0);

		if (winningNumber == m_number_1 || winningNumber == m_number_2
			|| winningNumber == m_number_3 || winningNumber == m_number_4)
		{
			result.win = true;
			result.amount = CalculatePrize();
			return result;
		}
		else
			return result;
	}

	Result FindNumberInStreet(int winningNumber)
	{
		Result result = new Result(false, 0);

		if (winningNumber == m_number_1 || winningNumber == m_number_2
			|| winningNumber == m_number_3)
		{
			result.win = true;
			result.amount = CalculatePrize();
			return result;
		}
		else
			return result;
	}

	Result FindNumberInSplit(int winningNumber)
	{
		Result result = new Result(false, 0);

		if (winningNumber == m_number_1 || winningNumber == m_number_2)
		{
			result.win = true;
			result.amount = CalculatePrize();
			return result;
		}
		else
			return result;
	}

	Result FindNumberInStraight(int winningNumber)
	{
		Result result = new Result(false, 0);

		if (winningNumber == m_number_1)
		{
			result.win = true;
			result.amount = CalculatePrize();
			return result;
		}
		else
			return result;
	}

	Result FindNumberInDozen(int winningNumber, int dozen)
	{
		Result result = new Result(false, 0);

		if (winningNumber == 0)
			return result;

		int dozenValue = GameManager.Instance.GameStats.DozenValue;

		if (winningNumber <= dozenValue * dozen &&
			winningNumber > (dozen - 1) * dozenValue)
		{
			result.win = true;
			result.amount = CalculatePrize();
		}
		else
			return result;

		return result;
	}

	Result FindNumberInLine(int winningNumber, int line)
	{
		Result result = new Result(false, 0);

		if (winningNumber == 0)
			return result;

		int counter = GameManager.Instance.GameStats.TableLines;
		int numbersCount = GameManager.Instance.GameStats.RouletteNumbers - 1;

		for (int i = line; i < numbersCount; i++)
		{
			if (counter == GameManager.Instance.GameStats.TableLines)
			{
				if (i == winningNumber)
				{
					result.win = true;
					result.amount = CalculatePrize();
					return result;
				}
			}

			counter--;

			if (counter == 0)
				counter = GameManager.Instance.GameStats.TableLines;
		}

		return result;
	}

	Result FindNumberInEven(int winningNumber)
	{
		Result result = new Result(false, 0);

		if (winningNumber == 0)
		{
			result.amount = Mathf.RoundToInt(m_betValue / 2);
			return result;
		}

		int numbersCount = GameManager.Instance.GameStats.RouletteNumbers - 1;

		for (int i = 0; i < numbersCount; i++)
		{
			if (i % 2 == 0 && i == winningNumber)
			{
				result.win = true;
				result.amount = CalculatePrize();
				return result;
			}
		}

		return result;
	}

	Result FindNumberInOdd(int winningNumber)
	{
		Result result = new Result(false, 0);

		if (winningNumber == 0)
		{
			result.amount = Mathf.RoundToInt(m_betValue / 2);
			return result;
		}

		int numbersCount = GameManager.Instance.GameStats.RouletteNumbers - 1;

		for (int i = 0; i < numbersCount; i++)
		{
			if (i % 2 != 0 && i == winningNumber)
			{
				result.win = true;
				result.amount = CalculatePrize();
				return result;
			}
		}

		return result;
	}

	Result FindNumberInHigh(int winningNumber)
	{
		Result result = new Result(false, 0);

		if (winningNumber == 0)
		{
			result.amount = Mathf.RoundToInt(m_betValue / 2);
			return result;
		}

		int numbersCount = GameManager.Instance.GameStats.RouletteNumbers;

		for (int i = 0; i < numbersCount; i++)
		{
			if (i > numbersCount / 2 && i == winningNumber)
			{
				result.win = true;
				result.amount = CalculatePrize();
				return result;
			}
		}

		return result;
	}

	Result FindNumberInLow(int winningNumber)
	{
		Result result = new Result(false, 0);

		if (winningNumber == 0)
		{
			result.amount = Mathf.RoundToInt(m_betValue / 2);
			return result;
		}

		int numbersCount = GameManager.Instance.GameStats.RouletteNumbers;

		for (int i = 0; i < numbersCount; i++)
		{
			if (i <= numbersCount / 2 && i == winningNumber)
			{
				result.win = true;
				result.amount = CalculatePrize();
				return result;
			}
		}

		return result;
	}

	Result FindNumberInRed(int winningNumber)
	{
		Result result = new Result(false, 0);

		if (winningNumber == 0)
		{
			result.amount = Mathf.RoundToInt(m_betValue / 2);
			return result;
		}

		int numbersCount = GameManager.Instance.GameStats.RouletteNumbers - 1;

		for (int i = 0; i < numbersCount; i++)
		{
			if (GameManager.Instance.GameStats.NumbersColor[i] ==
				Enums.InsideFieldColor.Red && i == winningNumber)
			{
				result.win = true;
				result.amount = CalculatePrize();
				return result;
			}
		}

		return result;
	}

	Result FindNumberInBlack(int winningNumber)
	{
		Result result = new Result(false, 0);

		if (winningNumber == 0)
		{
			result.amount = Mathf.RoundToInt(m_betValue / 2);
			return result;
		}

		int numbersCount = GameManager.Instance.GameStats.RouletteNumbers - 1;

		for (int i = 0; i < numbersCount; i++)
		{
			if (GameManager.Instance.GameStats.NumbersColor[i] ==
				Enums.InsideFieldColor.Black && i == winningNumber)
			{
				result.win = true;
				result.amount = CalculatePrize();
				return result;
			}
		}

		return result;
	}
}