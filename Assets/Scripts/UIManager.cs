﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
	[SerializeField] TMPro.TextMeshProUGUI m_currencyLabel;
	[SerializeField] TMPro.TextMeshProUGUI m_currentBetsValueLabel;
	[SerializeField] TMPro.TextMeshProUGUI m_lastWonAmount;
	[Space]
	[SerializeField] Button m_spinButton;
	[SerializeField] InsideField[] m_insideFields;
	[SerializeField] FieldBase[] m_outsideFields;

	[Header("Panel Result"), Space]
	[SerializeField] GameObject m_panelResult;
	[SerializeField] Image m_winImage;
	[SerializeField] Image m_noWinImage;
	[SerializeField] CanvasGroup m_panelResultCanvasGroup;

	[SerializeField] TMPro.TextMeshProUGUI m_prizeLabel;
	[SerializeField] TMPro.TextMeshProUGUI m_numberLabel;

	[Space]

	[SerializeField] float m_fadeResultPanelSmoothTime = .5f;
	[SerializeField] float m_resultPanelDestAlpha = 165f;
	[SerializeField] float m_timeToHideResultPanel = 1f;
	[SerializeField] float m_timeToShowResult = .5f;

	[Space]

	[SerializeField] Color32 m_redNumberColor;
	[SerializeField] Color32 m_whiteNumberColor;

	GameManager m_gameMan;

	private void Start()
	{
		m_gameMan = GameManager.Instance;

		m_gameMan.OnResult += ShowResult;
		m_gameMan.OnBet += UpdateBetsLabel;
		m_gameMan.OnReadyToBet += ReadyToBet;
		m_gameMan.OnReadyToSpin += ReadyToSpin;
		m_gameMan.OnBetAreaPointerEnter += HighlightFields;
		m_gameMan.OnPlayerCurrencyChange += OnCurrencyChange;
		m_gameMan.OnBetAreaPointerExit += FadeOutAllNumbersHighlights;
	}

	private void OnDestroy()
	{
		m_gameMan.OnResult -= ShowResult;
		m_gameMan.OnBet -= UpdateBetsLabel;
		m_gameMan.OnReadyToBet -= ReadyToBet;
		m_gameMan.OnReadyToSpin -= ReadyToSpin;
		m_gameMan.OnBetAreaPointerEnter -= HighlightFields;
		m_gameMan.OnPlayerCurrencyChange -= OnCurrencyChange;
		m_gameMan.OnBetAreaPointerExit -= FadeOutAllNumbersHighlights;
	}


	private void ShowResult(bool win, int prize, int number)
	{
		m_panelResult.SetActive(true);

		StartCoroutine(GUIUtils.FadeCanvasRoutine(m_panelResultCanvasGroup,
			m_fadeResultPanelSmoothTime, 0, m_resultPanelDestAlpha));

		m_numberLabel.color = GameManager.Instance.GameStats.GetNumberColor(number)
			== Enums.InsideFieldColor.Red ? m_redNumberColor : m_whiteNumberColor;

		m_numberLabel.text = number.ToString();

		StartCoroutine(ShowResultAndHidePanel(win, prize));
	}

	private IEnumerator ShowResultAndHidePanel(bool win, int prize)
	{
		yield return new WaitForSeconds(m_timeToShowResult);

		m_numberLabel.text = "";

		m_winImage.gameObject.SetActive(win);
		m_noWinImage.gameObject.SetActive(!win);

		m_prizeLabel.text = win ? prize.ToString() : "";

		yield return new WaitForSeconds(m_timeToHideResultPanel);

		yield return StartCoroutine(GUIUtils.FadeCanvasRoutine(m_panelResultCanvasGroup,
			m_fadeResultPanelSmoothTime, m_panelResultCanvasGroup.alpha, 0));

		m_panelResult.SetActive(false);
		m_winImage.gameObject.SetActive(false);
		m_noWinImage.gameObject.SetActive(false);
		m_prizeLabel.text = "";

		if (win)
			m_lastWonAmount.text = prize.ToString();

		GameManager.Instance.PrepareTableToGame();
	}

	private void ReadyToBet() => ReadyToSpin(false);

	private void ReadyToSpin(bool readyToSpin) => ActivateSpinButton(readyToSpin);

	private void OnCurrencyChange(int currency) => m_currencyLabel.text = currency.ToString();

	private void UpdateBetsLabel(int value) => m_currentBetsValueLabel.text = value.ToString();

	private void ActivateSpinButton(bool activate) => m_spinButton.interactable = activate;


	#region FIELD_HIGHLIGHT

	void HighlightFields(ShowBet showBet)
	{
		switch (showBet.betType)
		{
			case Enums.BetType.Dozen1:
				ShowDozenNumbers(1);
				break;
			case Enums.BetType.Dozen2:
				ShowDozenNumbers(2);
				break;
			case Enums.BetType.Dozen3:
				ShowDozenNumbers(3);
				break;
			case Enums.BetType.Line1:
				ShowLine(1);
				break;
			case Enums.BetType.Line2:
				ShowLine(2);
				break;
			case Enums.BetType.Line3:
				ShowLine(3);
				break;
			case Enums.BetType.High:
				ShowHighNumbers();
				break;
			case Enums.BetType.Low:
				ShowLowNumbers();
				break;
			case Enums.BetType.Even:
				ShowEvenNumbers();
				break;
			case Enums.BetType.Odd:
				ShowOddNumbers();
				break;
			case Enums.BetType.Red:
				ShowRedNumbers();
				break;
			case Enums.BetType.Black:
				ShowBlackNumbers();
				break;
			default:
				ShowInsideFieldsNumbers(showBet);
				break;
		}

		for (int i = 0; i < m_outsideFields.Length; i++)
		{
			if (m_outsideFields[i].FieldType == showBet.betType)
				m_outsideFields[i].OnHighlight(true);
		}
	}

	void ShowInsideFieldsNumbers(ShowBet showBet)
	{
		FadeOutAllNumbersHighlights();

		switch (showBet.betType)
		{
			case Enums.BetType.Straight:
				ShowInsideFields(new int[] { showBet.m_number1 });
				break;
			case Enums.BetType.Split:
				ShowInsideFields(new int[] { showBet.m_number1, showBet.m_number2 });
				break;
			case Enums.BetType.Street:
				ShowInsideFields(new int[] { showBet.m_number1, showBet.m_number2,
					showBet.m_number3 });
				break;
			case Enums.BetType.Corner:
				ShowInsideFields(new int[] { showBet.m_number1, showBet.m_number2,
					showBet.m_number3, showBet.m_number4});
				break;
			case Enums.BetType.SixLine:
				ShowInsideFields(new int[] { showBet.m_number1, showBet.m_number2,
					showBet.m_number3, showBet.m_number4, showBet.m_number5, showBet.m_number6});
				break;
		}

	}

	void ShowInsideFields(int[] numbers)
	{
		for (int i = 0; i < m_insideFields.Length; i++)
		{
			for (int j = 0; j < numbers.Length; j++)
			{
				if (numbers[j] == m_insideFields[i].Number)
					m_insideFields[m_insideFields[i].Number == 0 ? 0 : i].OnHighlight(true);
			}
		}
	}

	void ShowsSingleNumber(int number)
	{
		FadeOutAllNumbersHighlights();

		for (int i = 0; i < m_insideFields.Length; i++)
		{
			if (m_insideFields[i].Number == number)
				m_insideFields[i].OnHighlight(true);
		}
	}

	void ShowOddNumbers()
	{
		FadeOutAllNumbersHighlights();

		for (int i = 0; i < m_insideFields.Length; i++)
		{
			if (m_insideFields[i].Number % 2 != 0)
				m_insideFields[i].OnHighlight(true);
		}
	}

	void ShowEvenNumbers()
	{
		FadeOutAllNumbersHighlights();

		for (int i = 0; i < m_insideFields.Length; i++)
		{
			if (m_insideFields[i].Number % 2 == 0
				&& m_insideFields[i].Number != 0)
				m_insideFields[i].OnHighlight(true);
		}
	}

	void ShowRedNumbers()
	{
		FadeOutAllNumbersHighlights();

		for (int i = 0; i < m_insideFields.Length; i++)
		{
			if (m_insideFields[i].FieldColor == Enums.InsideFieldColor.Red)
				m_insideFields[i].OnHighlight(true);
		}
	}

	void ShowBlackNumbers()
	{
		FadeOutAllNumbersHighlights();

		for (int i = 0; i < m_insideFields.Length; i++)
		{
			if (m_insideFields[i].FieldColor == Enums.InsideFieldColor.Black)
				m_insideFields[i].OnHighlight(true);
		}
	}

	void ShowLowNumbers()
	{
		FadeOutAllNumbersHighlights();

		for (int i = 0; i < m_insideFields.Length; i++)
		{
			if (m_insideFields[i].Number <= m_insideFields.Length / 2
				&& m_insideFields[i].Number != 0)
				m_insideFields[i].OnHighlight(true);
		}
	}

	void ShowHighNumbers()
	{
		FadeOutAllNumbersHighlights();

		for (int i = 0; i < m_insideFields.Length; i++)
		{
			if (m_insideFields[i].Number > m_insideFields.Length / 2)
				m_insideFields[i].OnHighlight(true);
		}
	}

	void ShowDozenNumbers(int dozenIndex)
	{
		FadeOutAllNumbersHighlights();

		for (int i = 0; i < m_insideFields.Length; i++)
		{
			if (m_insideFields[i].Number <= dozenIndex * m_gameMan.GameStats.DozenValue &&
				m_insideFields[i].Number > (dozenIndex - 1) * m_gameMan.GameStats.DozenValue)
				m_insideFields[i].OnHighlight(true);
		}
	}

	void ShowLine(int lineIndex)
	{
		FadeOutAllNumbersHighlights();

		int counter = m_gameMan.GameStats.TableLines;

		for (int i = lineIndex; i < m_insideFields.Length; i++)
		{
			if (m_insideFields[i].Number == 0)
			{
				counter = 0;
				continue;
			}

			if (counter == m_gameMan.GameStats.TableLines)
				m_insideFields[i].OnHighlight(true);

			counter--;

			if (counter == 0)
				counter = m_gameMan.GameStats.TableLines;
		}
	}

	void FadeOutAllNumbersHighlights()
	{
		for (int i = 0; i < m_insideFields.Length; i++)
		{
			if (m_insideFields[i].IsHighlighted)
				m_insideFields[i].OnHighlight(false);
		}

		for (int i = 0; i < m_outsideFields.Length; i++)
		{
			if (m_outsideFields[i].IsHighlighted)
				m_outsideFields[i].OnHighlight(false);
		}
	}

	#endregion
}
