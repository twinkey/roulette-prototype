﻿using System.Collections.Generic;
using UnityEngine;

public class DragableChipsManager : MonoBehaviour
{
	[SerializeField] private GameObject m_dragableChip1Prefab;
	[SerializeField] private GameObject m_dragableChip2Prefab;
	[SerializeField] private GameObject m_dragableChip5Prefab;
	[SerializeField] private GameObject m_dragableChip25Prefab;
	[SerializeField] private GameObject m_dragableChip50Prefab;
	[Space]
	[SerializeField] private Transform m_dragableChip1SpawnTransform;
	[SerializeField] private Transform m_dragableChip2SpawnTransform;
	[SerializeField] private Transform m_dragableChip5SpawnTransform;
	[SerializeField] private Transform m_dragableChip25SpawnTransform;
	[SerializeField] private Transform m_dragableChip50SpawnTransform;

	[SerializeField] private DragableChip m_dragableChip1;
	[SerializeField] private DragableChip m_dragableChip2;
	[SerializeField] private DragableChip m_dragableChip5;
	[SerializeField] private DragableChip m_dragableChip25;
	[SerializeField] private DragableChip m_dragableChip50;

	GameStats m_gameStats;

	private List<DragableChip> m_dragableChipsReadyToUse = new List<DragableChip>();
	[SerializeField]  private List<DragableChip> m_placedChips = new List<DragableChip>();

	private void Start()
	{
		GameManager.Instance.OnCleanTable += RemoveChipsFromTable;
		GameManager.Instance.OnChipDrop += OnChipDrop;

		m_gameStats = GameManager.Instance.GameStats;

		SpawnChips();
	}

	private void OnDestroy()
	{
		GameManager.Instance.OnCleanTable -= RemoveChipsFromTable;
		GameManager.Instance.OnChipDrop -= OnChipDrop;
	}

	private void RemoveChipsFromTable()
	{
		for (int i = 0; i < m_placedChips.Count; i++)
			Destroy(m_placedChips[i].gameObject);

		m_placedChips.Clear();

		SpawnChips();
	}

	void SpawnChips()
	{
		int playerCurrency = GameManager.Instance.PlayerCurrency;

		if (playerCurrency >= m_gameStats.Chip1Value)
		{
			if (m_dragableChip1 == null)
				m_dragableChip1 = SpawnChip(m_dragableChip1Prefab, m_dragableChip1SpawnTransform);
			else
				m_dragableChip1.gameObject.SetActive(true);
		}
		else
		{
			if (m_dragableChip1 != null)
				m_dragableChip1.gameObject.SetActive(false);
		}

		if (playerCurrency >= m_gameStats.Chip2Value)
		{
			if (m_dragableChip2 == null)
				m_dragableChip2 = SpawnChip(m_dragableChip2Prefab, m_dragableChip2SpawnTransform);
			else
				m_dragableChip2.gameObject.SetActive(true);
		}
		else
		{
			if (m_dragableChip2 != null)
				m_dragableChip2.gameObject.SetActive(false);
		}

		if (playerCurrency >= m_gameStats.Chip5Value)
		{
			if (m_dragableChip5 == null)
				m_dragableChip5 = SpawnChip(m_dragableChip5Prefab, m_dragableChip5SpawnTransform);
			else
				m_dragableChip5.gameObject.SetActive(true);
		}
		else
		{
			if (m_dragableChip5 != null)
				m_dragableChip5.gameObject.SetActive(false);
		}

		if (playerCurrency >= m_gameStats.Chip25Value)
		{
			if (m_dragableChip25 == null)
				m_dragableChip25 = SpawnChip(m_dragableChip25Prefab, m_dragableChip25SpawnTransform);
			else
				m_dragableChip25.gameObject.SetActive(true);
		}
		else
		{
			if (m_dragableChip25 != null)
				m_dragableChip25.gameObject.SetActive(false);
		}

		if (playerCurrency >= m_gameStats.Chip50Value)
		{
			if (m_dragableChip50 == null)
				m_dragableChip50 = SpawnChip(m_dragableChip50Prefab, m_dragableChip50SpawnTransform);
			else
				m_dragableChip50.gameObject.SetActive(true);
		}
		else
		{
			if (m_dragableChip50 != null)
				m_dragableChip50.gameObject.SetActive(false);
		}
	}

	private void OnChipDrop(DragableChip dragableChip)
	{
		m_placedChips.Add(dragableChip);

		switch (dragableChip.ChipValue)
		{
			case 1:
				m_dragableChip1 = null;
				break;
			case 2:
				m_dragableChip2 = null;
				break;
			case 5:
				m_dragableChip5 = null;
				break;
			case 25:
				m_dragableChip25 = null;
				break;
			case 50:
				m_dragableChip50 = null;
				break;
		}

		SpawnChips();
	}

	DragableChip SpawnChip(GameObject chipPrefab, Transform transform)
	{
		GameObject chipObject = Instantiate(chipPrefab, transform);
		DragableChip dragableChip = chipObject.GetComponent<DragableChip>();
		dragableChip.Active = true;

		return dragableChip;
	}
}
