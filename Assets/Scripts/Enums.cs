﻿public class Enums
{
	public enum BetType
	{
		Straight,
		Split,
		Street,
		Corner,
		SixLine,
		Even,
		Odd,
		Black,
		Red,
		Low,
		High,
		Dozen1,
		Dozen2,
		Dozen3,
		Line1,
		Line2,
		Line3
	}

	public enum InsideFieldColor
	{
		Red,
		Black,
		None
	}

	public enum GameStates
	{
		ReadyToBet,
		ChipChosen,
		Bet,
		Spin,
		Score
	}
}
