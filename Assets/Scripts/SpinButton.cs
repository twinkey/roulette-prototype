﻿using UnityEngine;

public class SpinButton : MonoBehaviour
{
	public void OnSpinButton() => GameManager.Instance.Spin();
}
