﻿using UnityEngine;
using UnityEngine.EventSystems;

public class BetAreaBase : MonoBehaviour, IDropHandler, IPointerEnterHandler, IPointerExitHandler
{
	[SerializeField] protected Enums.BetType m_areaType;

	protected DragableChip m_dragableChip;

	private void Start()
	{
		GameManager.Instance.OnChipDrop += OnDropChip;
	}

	private void OnDestroy()
	{
		GameManager.Instance.OnChipDrop -= OnDropChip;
	}

	private void OnDropChip(DragableChip dragableChip) => m_dragableChip = null;

	public virtual void OnPointerEnter(PointerEventData eventData)
	{
		if (eventData.pointerDrag == null || GameManager.Instance.CurrentGameState != Enums.GameStates.ChipChosen)
			return;

		m_dragableChip = eventData.pointerDrag.GetComponent<DragableChip>();
	}

	public void OnPointerExit(PointerEventData eventData)
	{
		if (eventData.pointerDrag == null || GameManager.Instance.CurrentGameState != Enums.GameStates.ChipChosen)
			return;

		m_dragableChip = eventData.pointerDrag.GetComponent<DragableChip>();
		if (m_dragableChip != null)
			GameManager.Instance.BetAreaPointerExit();
	}

	public void OnDrop(PointerEventData eventData)
	{
		m_dragableChip = eventData.pointerDrag.GetComponent<DragableChip>();

		if (m_dragableChip != null)
		{
			m_dragableChip.transform.parent = this.transform;
			m_dragableChip.SetChipPosition(Vector2.zero);
			m_dragableChip.Active = false;

			OnBet();
		}
	}

	public virtual void OnBet() { }
}