﻿using UnityEngine.EventSystems;
using UnityEngine;

public class BetAreaInside : BetAreaBase
{
	[SerializeField] public InsideField[] m_insideFields = new InsideField[6];

	public override void OnPointerEnter(PointerEventData eventData)
	{
		base.OnPointerEnter(eventData);

		if (m_dragableChip == null)
			return;

		GameManager.Instance.BetAreaPointerEnter(m_areaType, m_insideFields[0] != null ? m_insideFields[0].Number : -1,
		m_insideFields[1] != null ? m_insideFields[1].Number : -1, m_insideFields[2] != null ? m_insideFields[2].Number : -1,
		m_insideFields[3] != null ? m_insideFields[3].Number : -1, m_insideFields[4] != null ? m_insideFields[4].Number : -1,
		m_insideFields[5] != null ? m_insideFields[5].Number : -1);
	}

	public override void OnBet()
	{
		base.OnBet();

		GameManager.Instance.CreateBet(m_areaType, m_insideFields[0] != null ? m_insideFields[0].Number : -1,
		m_insideFields[1] != null ? m_insideFields[1].Number : -1, m_insideFields[2] != null ? m_insideFields[2].Number : -1,
		m_insideFields[3] != null ? m_insideFields[3].Number : -1, m_insideFields[4] != null ? m_insideFields[4].Number : -1,
		m_insideFields[5] != null ? m_insideFields[5].Number : -1);
	}
}
