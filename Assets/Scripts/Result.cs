﻿public struct Result
{
	public bool win;
	public int amount;

	public Result( bool NewWin, int NewAmount)
	{
		win = NewWin;
		amount= NewAmount;
	}
}
