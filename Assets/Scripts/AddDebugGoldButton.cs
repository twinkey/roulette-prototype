﻿using UnityEngine;

public class AddDebugGoldButton : MonoBehaviour
{
	public void AddGold() => GameManager.Instance.AddDebugGold();
}
