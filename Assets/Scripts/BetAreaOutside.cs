﻿using UnityEngine.EventSystems;

public class BetAreaOutside : BetAreaBase
{
	public override void OnPointerEnter(PointerEventData eventData)
	{
		base.OnPointerEnter(eventData);

		if (m_dragableChip != null)
			GameManager.Instance.BetAreaPointerEnter(m_areaType);
	}

	public override void OnBet()
	{
		base.OnBet();
		GameManager.Instance.CreateBet(m_areaType);
	}
}

