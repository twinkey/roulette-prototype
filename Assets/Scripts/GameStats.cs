﻿using UnityEngine;

public class GameStats : ScriptableObject
{
	[SerializeField] private int m_dozenValue = 12;
	public int DozenValue => m_dozenValue;

	[Space]

	[SerializeField] private int m_rouletteNumbers = 36;
	public int RouletteNumbers => m_rouletteNumbers;

	[SerializeField] Enums.InsideFieldColor[] m_numbersColor;
	public Enums.InsideFieldColor[] NumbersColor => m_numbersColor;

	[SerializeField] private int m_tableLines = 3;
	public int TableLines => m_tableLines;

	[SerializeField] private int m_playerStartCurrency = 200;
	public int PlayerStartCurrency => m_playerStartCurrency;

	[SerializeField] private int m_addDebugCoinsValue = 100;
	public int DebugCoinsValue => m_addDebugCoinsValue;

	[SerializeField] private int m_playerMaxCurrency = 1000;
	public int PlayerMaxCurrency => m_playerMaxCurrency;

	[Space]

	[SerializeField] private int m_chip1Value = 1;
	public int Chip1Value => m_chip1Value;

	[SerializeField] private int m_chip2Value = 2;
	public int Chip2Value => m_chip2Value;

	[SerializeField] private int m_chip5Value = 5;
	public int Chip5Value => m_chip5Value;

	[SerializeField] private int m_chip25Value = 25;
	public int Chip25Value => m_chip25Value;

	[SerializeField] private int m_chip50Value = 50;
	public int Chip50Value => m_chip50Value;

	[Space, Header("Payouts 1 : X")]

	[SerializeField] private int m_dozenPayout = 2;
	public int DozenPayout => m_dozenPayout;

	[SerializeField] private int m_linePayout = 2;
	public int LinePayout => m_linePayout;

	[SerializeField] private int m_evenPayout = 1;
	public int EvenPayout => m_evenPayout;

	[SerializeField] private int m_oddPayout = 1;
	public int OddPayout => m_oddPayout;

	[SerializeField] private int m_highPayout = 1;
	public int HighPayout => m_highPayout;

	[SerializeField] private int m_lowPayout = 1;
	public int LowPayout => m_lowPayout;

	[SerializeField] private int m_redPayout = 1;
	public int RedPayout => m_redPayout;

	[SerializeField] private int m_blackPayout = 1;
	public int BlackPayout => m_blackPayout;

	[SerializeField] private int m_straightPayout = 35;
	public int StraightPayout => m_straightPayout;

	[SerializeField] private int m_splitPayout = 17;
	public int SplitPayout => m_splitPayout;

	[SerializeField] private int m_cornerPayout = 8;
	public int CornerPayout => m_cornerPayout;

	[SerializeField] private int m_streetPayout = 11;
	public int StreetPayout => m_streetPayout;

	[SerializeField] private int m_sixlinePayout = 5;
	public int SixlinePayout => m_sixlinePayout;


	public Enums.InsideFieldColor GetNumberColor(int number)
	{
		return m_numbersColor[number];
	}
}
