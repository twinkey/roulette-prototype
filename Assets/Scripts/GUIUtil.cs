﻿using System.Collections;
using UnityEngine.UI;
using UnityEngine;

public static class GUIUtils
{
	#region COROUTINES
	public static IEnumerator FadeCanvasRoutine(CanvasGroup canvasGroup, float smoothTime, float startAlpha, float destAlfa, float startDelay = 0)
	{
		yield return new WaitForSeconds(startDelay);

		float time = 0;
		float invertedSmooth = 1 / smoothTime;

		while (time < smoothTime)
		{
			float newAlpha = Mathf.Lerp(startAlpha, destAlfa, time * invertedSmooth);
			time += UnityEngine.Time.deltaTime;
			canvasGroup.alpha = newAlpha;
			yield return null;
		}

		canvasGroup.alpha = destAlfa;
	}

	public static IEnumerator RotateImageRoutine(Image image, float start, float end, float smoothTime, float startDelay)
	{
		yield return new WaitForSeconds(startDelay);

		float time = 0;
		float invertedSmooth = 1 / smoothTime;

		while (time < smoothTime)
		{
			image.rectTransform.rotation = Quaternion.Euler(0f, 0f, Mathf.Lerp(start, end, time * invertedSmooth));
			time += UnityEngine.Time.deltaTime;
			yield return null;
		}
	}

	public static IEnumerator FillAmountImageRoutine(Image image, float start, float end, float smooth, float startDelay)
	{
		yield return new WaitForSeconds(startDelay);

		float time = 0;
		float invertedSmooth = 1 / smooth;

		while (time < smooth)
		{
			float newValue = Mathf.Lerp(start, end, time * invertedSmooth);
			image.fillAmount = newValue;
			time += Time.deltaTime;
			yield return null;
		}
	}

	public static IEnumerator MoveCanvasObjectToPositioRoutinen(RectTransform rectObject, Vector2 startPos, Vector2 destPos, float smoothTime, float startDelay)
	{
		yield return new WaitForSeconds(startDelay);

		if (smoothTime <= 0)
		{
			rectObject.localPosition = destPos;
		}
		else
		{
			float time = 0;
			float invertedSmooth = 1 / smoothTime;

			while (time < smoothTime)
			{
				Vector2 _newRect = Vector2.Lerp(startPos, destPos, time * invertedSmooth);
				rectObject.localPosition = _newRect;
				time += Time.deltaTime;
				yield return null;
			}
		}
	}

	public static IEnumerator ScaleRectRoutine(RectTransform rect, Vector2 startScale, Vector2 destScale, float smoothTime, float startDelay = 0)
	{
		yield return new WaitForSeconds(startDelay);

		float time = 0;

		if (smoothTime > 0)
		{
			float invertedSmooth = 1 / smoothTime;

			while (time < smoothTime)
			{
				Vector2 newScale = Vector2.Lerp(startScale, destScale, time * invertedSmooth);
				rect.localScale = newScale;
				time += Time.deltaTime;
				yield return null;
			}

			rect.localScale = destScale;
		}
	}

	public static IEnumerator FadeImageRoutine(Image image, float startAlpha, float destAlpha, float smoothTime, float startDelay = 0)
	{
		yield return new WaitForSeconds(startDelay);

		float time = 0;
		Color c = image.color;

		if (smoothTime > 0)
		{
			float invertedSmooth = 1 / smoothTime;

			while (time < smoothTime)
			{
				float newAlpha = Mathf.Lerp(startAlpha, destAlpha, time * invertedSmooth);
				c.a = newAlpha;
				image.color = c;

				time += Time.deltaTime;
				yield return null;
			}
		}
	}

	#endregion
}