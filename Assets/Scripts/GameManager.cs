﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : SingletonBase<GameManager>
{
	private Enums.GameStates m_currentGameState;
	public Enums.GameStates CurrentGameState => m_currentGameState;

	[SerializeField] private GameStats m_gameStats;
	public GameStats GameStats => m_gameStats;

	public List<Bet> m_bets = new List<Bet>();

	private int m_playerCurrency = 200;
	public int PlayerCurrency => m_playerCurrency;

	private int m_currenSelectedChipValue;
	private int m_currenBetsValue;

	#region Delagates

	public event Action<ShowBet> OnBetAreaPointerEnter;
	public event Action<int> OnPlayerCurrencyChange;
	public event Action<bool, int, int> OnResult;
	public event Action<DragableChip> OnChipDrop;
	public event Action OnBetAreaPointerExit;
	public event Action<bool> OnReadyToSpin;
	public event Action OnCleanTable;
	public event Action OnReadyToBet;
	public event Action<int> OnBet;

	#endregion

	private void Start() => PrepareTableToGame();

	public void CreateBet(Enums.BetType betType, int number_1 = -1, int number_2 = -1,
		int number_3 = -1, int number_4 = -1, int number_5 = -1, int number_6 = -1)
	{
		m_currentGameState = Enums.GameStates.Bet;
		BetAreaPointerExit();
		ChangePlayerCurrency(-m_currenSelectedChipValue);

		m_bets.Add(new Bet(m_currenSelectedChipValue, number_1, 
			number_2, number_3, number_4, number_5, number_6, betType));

		ReadyToBet();

		if (m_bets.Count > 0)
			CallOnReadyToSpin(true);

		m_currenBetsValue += m_currenSelectedChipValue;
		CallOnBet();
	}

	public void ChipChosen(int chipValue)
	{
		m_currenSelectedChipValue = chipValue;
		m_currentGameState = Enums.GameStates.ChipChosen;
	}

	public void DropChip(DragableChip dragableChip)
	{
		m_currentGameState = Enums.GameStates.ReadyToBet;
		BetAreaPointerExit();

		OnChipDrop?.Invoke(dragableChip);
	}

	public void PrepareTableToGame()
	{
		CallOnCurrencyChange();
		CallOnReadyToSpin(false);
		ReadyToBet();

		OnCleanTable?.Invoke();
	}

	void ChangePlayerCurrency(int value)
	{
		m_playerCurrency = Mathf.Clamp(m_playerCurrency + value, 0, m_gameStats.PlayerMaxCurrency);
		CallOnCurrencyChange();
	}

	void ReadyToBet()
	{
		m_currentGameState = Enums.GameStates.ReadyToBet;
		OnReadyToBet?.Invoke();
	}

	public void Spin()
	{
		if (m_currentGameState == Enums.GameStates.ReadyToBet)
		{
			m_currentGameState = Enums.GameStates.Spin;

			int randomizedNumber = GetRandomRouletteNumber();
			CheckForWinnings(randomizedNumber);
		}
	}

	private void CheckForWinnings(int number)
	{
		bool win = false;
		int prizeTotal = 0;

		for (int i = 0; i < m_bets.Count; i++)
		{
			Result result = m_bets[i].CheckBet(number);

			if (result.win)
			{
				prizeTotal += result.amount;
				win = true;
			}
		}

		m_bets.Clear();
		m_currenBetsValue = 0;

		CallOnBet();

		OnResult?.Invoke(win, prizeTotal, number);
		ChangePlayerCurrency(prizeTotal);
	}

	private int GetRandomRouletteNumber()
	{
		return UnityEngine.Random.Range(0, m_gameStats.RouletteNumbers);
	}

	public void BetAreaPointerExit()
	{
		OnBetAreaPointerExit?.Invoke();
	}

	private void CallOnBet() => OnBet?.Invoke(m_currenBetsValue);

	private void CallOnCurrencyChange() => OnPlayerCurrencyChange?.Invoke(m_playerCurrency);

	private void CallOnReadyToSpin(bool readyToSpin) => OnReadyToSpin?.Invoke(readyToSpin);

	public void BetAreaPointerEnter(Enums.BetType betType) => OnBetAreaPointerEnter?.Invoke(new ShowBet(betType));

	public void BetAreaPointerEnter(Enums.BetType betType, int m_number1, int m_number2, int m_number3, int m_number4, int m_number5, int m_number6)
	{
		OnBetAreaPointerEnter?.Invoke(new ShowBet(betType, m_number1, m_number2, m_number3, m_number4, m_number5, m_number6));
	}

	#region DEBUG
	public void AddDebugGold()
	{
		int lastCurrency = m_playerCurrency;
		ChangePlayerCurrency(m_gameStats.DebugCoinsValue);

		if (lastCurrency == 0)
			PrepareTableToGame();
	}
	#endregion
}