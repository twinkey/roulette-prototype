﻿using UnityEngine.UI;
using UnityEngine;

public class FieldBase : MonoBehaviour
{
	[SerializeField] Image m_highlightImage;
	[SerializeField] float m_fadeHighlighImageSpeed = .2f;
	[SerializeField] float m_fadeDestAlpha = .4f;

	[SerializeField] Enums.BetType m_fieldType;
	public Enums.BetType FieldType => m_fieldType;

	private bool m_isHighlighted;
	public bool IsHighlighted;

	private Coroutine m_fadeRoutine;

	public void OnHighlight(bool highlight)
	{
		if (m_fadeRoutine != null)
			StopCoroutine(m_fadeRoutine);

		m_fadeRoutine = StartCoroutine(GUIUtils.FadeImageRoutine(m_highlightImage, highlight ? m_highlightImage.color.a : m_fadeDestAlpha,
		highlight ? m_fadeDestAlpha : 0, m_fadeHighlighImageSpeed));
		IsHighlighted = highlight;
	}

}