﻿public struct ShowBet
{
	public Enums.BetType betType;
	public int m_number1, m_number2, m_number3, m_number4, m_number5, m_number6;

	public ShowBet(Enums.BetType NewBetType, int NewNumber1 = -1, int NewNumber2 = -1,
		int NewNumber3 = -1, int NewNumber4 = -1, int NewNumber5 = -1, int NewNumber6 = -1)
	{
		betType = NewBetType;
		m_number1 = NewNumber1;
		m_number2 = NewNumber2;
		m_number3 = NewNumber3;
		m_number4 = NewNumber4;
		m_number5 = NewNumber5;
		m_number6 = NewNumber6;
	}
}
