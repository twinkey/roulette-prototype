﻿using UnityEngine.EventSystems;
using UnityEngine;

public class DragableChip : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
	[SerializeField] CanvasGroup m_canvasGroup;
	[SerializeField] Vector2 m_chipScaleOnDrag = new Vector2(.5f, .5f);
	[SerializeField] Vector2 m_offset = new Vector2(0, 1f);

	[Space]

	[SerializeField] int m_chipValue;
	public int ChipValue => m_chipValue;

	[SerializeField] bool m_active = true;
	public bool Active
	{
		get { return m_active; }
		set { m_active = value; }
	}

	private Transform m_returnparent = null;
	public Transform ReturnParent
	{
		get { return m_returnparent; }
		set { m_returnparent = value; }
	}

	private Transform placeholderParent = null;

	GameObject placeholder = null;

	public void OnBeginDrag(PointerEventData eventData)
	{
		if (!m_active)
			return;

		GameManager.Instance.ChipChosen(m_chipValue);

		placeholder = new GameObject();
		placeholder.transform.SetParent(this.transform.parent);

		placeholder.transform.SetSiblingIndex(this.transform.GetSiblingIndex());

		m_returnparent = this.transform.parent;
		placeholderParent = m_returnparent;
		this.transform.SetParent(this.transform.parent.parent);

		m_canvasGroup.blocksRaycasts = false;

		ChangeChipScale(m_chipScaleOnDrag);
	}

	public void OnDrag(PointerEventData eventData)
	{
		if (!m_active)
			return;

		this.transform.position = m_offset + eventData.position;

		if (placeholder.transform.parent != placeholderParent)
			placeholder.transform.SetParent(placeholderParent);

		int newSiblingIndex = placeholderParent.childCount;

		for (int i = 0; i < placeholderParent.childCount; i++)
		{
			if (this.transform.position.x < placeholderParent.GetChild(i).position.x)
			{
				newSiblingIndex = i;

				if (placeholder.transform.GetSiblingIndex() < newSiblingIndex)
					newSiblingIndex--;

				break;
			}
		}

		placeholder.transform.SetSiblingIndex(newSiblingIndex);
	}

	public void OnEndDrag(PointerEventData eventData)
	{
		Destroy(placeholder);

		if (!m_active)
		{
			GameManager.Instance.DropChip(this);
			return;
		}

		this.transform.SetParent(m_returnparent);
		this.transform.SetSiblingIndex(placeholder.transform.GetSiblingIndex());

		m_canvasGroup.blocksRaycasts = true;

		transform.GetComponent<RectTransform>().anchoredPosition = Vector3.zero;

		ChangeChipScale(Vector2.one);
	}

	void ChangeChipScale(Vector2 scale) => transform.localScale = scale;

	public void SetChipPosition(Vector2 newPosition) => transform.GetComponent<RectTransform>().anchoredPosition = newPosition;
}