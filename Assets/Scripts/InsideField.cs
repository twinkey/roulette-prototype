﻿using UnityEngine;

public class InsideField : FieldBase
{
	Enums.InsideFieldColor m_fieldColor;
	public Enums.InsideFieldColor FieldColor => m_fieldColor;

	[SerializeField] int m_number;
	public int Number => m_number;

	private void Start() => m_fieldColor = GameManager.Instance.GameStats.NumbersColor[m_number];

}
