# Roulette game prototype

The application is designed to simulate basic game functionalities such as: dragging chips, creating bets , viewing 
winning numer and prize.

I started the work by analyzing the mechanics of the game. First, I programmed the board behavior 
(highlighting boxes, adding chips and taking bets positions), and later I added a reward calculation system.

If I had more time, I would add:

1.  Mechanics of the roulette wheel
2.  Undo player moves
3.  Pooling chips system
4.  Sound effects
5.  Animations

## Built With

To open the project you need to have the Unity editor version 2018.3.4f1

Available for download at: [Unity download archive](https://unity3d.com/get-unity/download/archive)

## Sample Gameplay
![enter image description here](https://i.ibb.co/xSXntNz/ezgif-com-video-to-gif.gif)


##  Authors

[Bartłomiej Siekacz ](https://www.linkedin.com/in/bartekczybartosz)
